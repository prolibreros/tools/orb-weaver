require_relative 'lib/orb-weaver/version'

Gem::Specification.new do |spec|
  spec.name          = "orb-weaver"
  spec.version       = OrbWeaver::VERSION
  spec.authors       = ["Programando LIBREros, Perro Tuerto"]
  spec.email         = ["hi@programando.li"]

  spec.summary       = %q{Abstract Syntax Tree deployer}
  spec.description   = %q{Work with assets and let the spider weave AST, catch, wrap and liquify code for websites, apps, games and epublications.}
  spec.homepage      = "https://gitlab.com/programando-libreros/herramientas/orb-weaver"
  spec.license       = "GPL-3.0-or-later"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.5.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/programando-libreros/herramientas/orb-weaver"
  spec.metadata["changelog_uri"] = "https://gitlab.com/programando-libreros/herramientas/orb-weaver/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rspec", "~> 3.9"
  spec.add_dependency "i18n", "~> 1.8.3"
  spec.add_dependency "vine", "~> 0.4"
  spec.add_dependency "tty-markdown", "~> 0.7.0"
  spec.add_dependency "filewatcher", "~> 1.1.1", ">= 1.1.1"
  spec.add_dependency "ruby-filemagic", "~> 0.7.2"
end
