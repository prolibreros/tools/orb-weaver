require 'i18n'
require 'fileutils'
require 'tty-markdown'
require 'filemagic'

$tool    = 'orb-weaver'
$cmds    = ['version', 'help', 'new', 'deploy', 'watch']
$root    = File.expand_path(__dir__ + '/../..') + '/'
$assets  = 'assets/'
$presets = 'presets/'
$minify  = false
$out     = 'outputs/'

def flatten_all hash
  hash.flatten(-1).map{|e| e.is_a?(Hash) ? flatten_all(e) : e}.flatten
end

def lang?
  sys_lang = ENV['LANG'] ? ENV['LANG'].split('_').first : nil
  if sys_lang && File.file?($root + "locales/#{sys_lang}.yml")
    sys_lang
  else
    'es'
  end
end

def text?(filename)
  begin
    fm = FileMagic.new(FileMagic::MAGIC_MIME)
    fm.file(filename) =~ /^text\//
  ensure
    fm.close
  end
end

def prt el, opt = nil
  content = opt && opt[:string] ? el : I18n.t(el.to_sym)
  content = content.is_a?(Array) ? content.join("\n") : content  
  if opt && opt[:replace] then content.gsub!(/\$\w+/, opt[:replace]) end
  puts TTY::Markdown.parse(content.to_s.force_encoding('utf-8'))
end

def prt_err err, extra = true
# TODO: canvas_name is a mess because @canvas is a mess; new structure needed
  canvas_name = @canvas[:metas].reject{|m| !m.has_key?(:id)}.first[:id].first
  canvas_file = extra ? canvas_name : ''
  prt('error_' + err, {:replace => canvas_file})
  if extra then puts "  #{@a.to_s}" end
  abort
end

def prt_help
  help = File.read($root + "locales/help.#{lang?}.md")
  prt(help, {:string => true})
end

def save path, content
  file = File.open(path, 'w:utf-8')
  file.puts content
  file.close
end

def mkdir dir, purge = false
  if !Dir.exist?(dir)
    Dir.mkdir(dir)
  elsif purge 
    FileUtils.rm_rf(dir)
    mkdir(dir)
  end
end

def verify_project
  if !File.exist?($assets)
    prt('error_project', {:replace => $tool})
    abort
  end
end

# Verifies canvases aviability;
# purged rejects .rb files that aren't canvases;
# returns canvases so al .rb files can be loaded
def verify_canvases
  canvases = Dir.glob($assets + '*.rb')
  purged   = canvases.reject{|c| File.basename(c) =~ /^_/}
  if purged.length == 0 then prt_err('canvases', false)
  else canvases end
end

def verify_file file, copy = false
  exist = File.exist?(file)
  print = false
  if !copy && !exist then print = 'error_missing'
  elsif copy && exist then print = 'error_duplicate'
  end

  if print
    prt(print, {:replace => file})
    abort
  else file
  end
end

def verify_pars pars
  if !ARGV[1] || !pars.include?(ARGV[1])
    prt(!ARGV[1] ? 'no_par1' : 'no_par2')
    abort
  else ARGV[1] end
end

I18n.load_path << Dir[$root + 'locales/*.yml']
I18n.default_locale = lang?
