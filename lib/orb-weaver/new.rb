require 'securerandom'

module OrbWeaver
  class New
    @@pars = ['project', 'canvas']

    def initialize name = nil
      @par  = name ? 'canvas' : verify_pars(@@pars)
      @name = new_name(name)
      @lang = I18n.default_locale.to_s
      send('new_' + @par)
      prt('new_' + @par, {:replace => @name})
    end

    def get_preset
      File.read($root + 'presets/' + @par + '.rb')
          .gsub(':lang[]', @lang)
          .gsub(':id[]', File.basename(@name, '.*'))
    end

    def new_name name
      ext = @par == 'project' ? '' : '.rb'
      ran = @par + '_' + SecureRandom.hex(2) + ext
      name ? File.basename(name, '.*') + ext : ARGV[2] ? ARGV[2] + ext : ran
    end

    def new_project
      path = @name + '/' + @name + '.rb'
      verify_file(@name, true)
      Dir.mkdir(@name)
      Dir.mkdir(@name + '/' + $assets)
      save(path, get_preset)
    end

    def new_canvas
      path = $assets + @name
      verify_project
      verify_file(path, true)
      save(path, get_preset)
    end
  end
end
