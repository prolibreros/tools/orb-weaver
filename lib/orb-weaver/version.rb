module OrbWeaver
  VERSION = "0.1.0"

  class Version
    def initialize
      prt(OrbWeaver::VERSION, {:string => true})
    end
  end
end
